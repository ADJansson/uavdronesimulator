﻿using OpenTK.Mathematics;
using OpenTK.Windowing.Common;

namespace UAVSimulator_OpenTK
{
    public class Scene : WorldObject
    {
        public Scene(Window window) : base(window)
        {

        }

        protected override void PrivateLoad()
        {
            
        }

        protected override void PrivateRender(FrameEventArgs e)
        {
            
        }

        protected override void PrivateUpdate(FrameEventArgs e)
        {
            
        }
    }
}
