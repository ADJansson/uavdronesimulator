﻿#version 330 core

layout(location = 0) in vec3 aPosition;

out vec3 inputColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 aInputColor;

void main(void)
{
    gl_Position = vec4(aPosition, 1.0) * model * view * projection;
    inputColor = aInputColor;
}