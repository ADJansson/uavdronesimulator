﻿
using OpenTK.Graphics.ES11;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAVSimulator_OpenTK
{
    public abstract class WorldObject
    {
        protected Matrix4 _model;
        protected Window _window;

        protected float _pitch = 0;
        protected float _yaw = 0;// -MathHelper.PiOver2;

        protected Vector3 _front = -Vector3.UnitZ;
        protected Vector3 _up = Vector3.UnitY;
        protected Vector3 _right = Vector3.UnitX;

        public Vector3 Front => _front;
        public Vector3 Up => _up;
        public Vector3 Right => _right;

        private List<WorldObject> _children;

        public Vector3 Position { get; set; }

        public WorldObject(Window window)
        {
            _window = window;
            _model = Matrix4.Identity;
            _children = new List<WorldObject>();
        }

        public void OnLoad()
        {
            PrivateLoad();
            foreach (var child in _children)
            {
                child.OnLoad();
            }
        }

        public void Render(FrameEventArgs e, Matrix4 parentMatrix)
        {
            Matrix4 temp = _model;
            _model = _model * parentMatrix;
            PrivateRender(e);
            foreach (var child in _children)
            {
                child.Render(e, _model);
            }
            _model = temp;
        }

        public void Update(FrameEventArgs e)
        {
            PrivateUpdate(e);
            foreach (var child in _children)
            {
                child.Update(e);
            }
        }

        protected abstract void PrivateLoad();
        protected abstract void PrivateRender(FrameEventArgs e);
        protected abstract void PrivateUpdate(FrameEventArgs e);

        public void AddChild(WorldObject child)
        {
            _children.Add(child);
        }

        public float Pitch
        {
            get => MathHelper.RadiansToDegrees(_pitch);
            set
            {
                var angle = MathHelper.Clamp(value, -89f, 89f);
                _pitch = MathHelper.DegreesToRadians(angle);
                UpdateVectors();
            }
        }

        public float Yaw
        {
            get => MathHelper.RadiansToDegrees(_yaw);
            set
            {
                _yaw = MathHelper.DegreesToRadians(value);
                UpdateVectors();
            }
        }

        protected void UpdateVectors()
        {
            _front.X = MathF.Cos(_pitch) * MathF.Cos(_yaw);
            _front.Y = MathF.Sin(_pitch);
            _front.Z = MathF.Cos(_pitch) * MathF.Sin(_yaw);

            _front = Vector3.Normalize(_front);
            _right = Vector3.Normalize(Vector3.Cross(_front, Vector3.UnitY));
            _up = Vector3.Normalize(Vector3.Cross(_right, _front));
        }
    }
}
