﻿using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;

namespace UAVSimulator_OpenTK
{
    public static class Program
    {
        private static void Main()
        {
            var nativeWindowSettigs = new NativeWindowSettings()
            {
                Size = new Vector2i(1280, 720),
                Title = "UAV Simulator 0.6.9",
                Flags = ContextFlags.ForwardCompatible,
            };

            using var window = new Window(GameWindowSettings.Default, nativeWindowSettigs);
            window.Run();
        }
    }
}
