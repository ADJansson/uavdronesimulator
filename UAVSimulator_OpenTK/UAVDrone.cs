﻿using OpenTK.Graphics.OpenGL4;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using System;

namespace UAVSimulator_OpenTK
{
    public class UAVDrone : WorldObject
    {
        private readonly float[] _vertices =
        {
            -0.24f, -0.06f, 0.1f,
            -0.04f, 0.0f, 0.0f,
            0.0f, 0.0f, -0.32f,
            0.0f, 0.04f, 0.0f,
            0.0f, -0.04f, 0.0f,
            0.04f, 0.0f, 0.0f,
            0.24f, -0.06f, 0.10f
        };

        private readonly uint[] _indices =
        {
            0, 1, 2,
            5, 6, 2,
            1, 3, 2,
            3, 5, 2,
            1, 4, 3,
            3, 4, 5
        };

        private int _vertexBufferObject;
        private int _vertexArrayObject;
        private int _elementBufferObject;

        private Shader _solidColorShader;
        private double _time;

        private double posX;
        private double posY;
        private double posZ;

        private Vector3 _color;
        private Random _rand;
        public UAVDrone(Window window) : base (window)
        {
            _rand = new Random();
            _color = new Vector3((float)_rand.NextDouble(), (float)_rand.NextDouble(), (float)_rand.NextDouble());

            Yaw = _rand.Next(-90, 90);
            Position += new Vector3(_rand.Next(-2, 2), _rand.Next(-2, 2), _rand.Next(-2, 2));

            _time += _rand.NextDouble() * 10;

            //AddChild(new SpinningPlane(window));
        }

        protected override void PrivateLoad()
        {
            _vertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(_vertexArrayObject);

            _vertexBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, _vertices.Length * sizeof(float), _vertices, BufferUsageHint.StaticDraw);

            _elementBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _elementBufferObject);
            GL.BufferData(BufferTarget.ElementArrayBuffer, _indices.Length * sizeof(uint), _indices, BufferUsageHint.StaticDraw);

            _solidColorShader = new Shader("Shaders/solidcolor.vert", "Shaders/solidcolor.frag");
            _solidColorShader.Use();

            var vertexLocation = _solidColorShader.GetAttribLocation("aPosition");
            GL.EnableVertexAttribArray(vertexLocation);
            GL.VertexAttribPointer(vertexLocation, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);
        }

        protected override void PrivateRender(FrameEventArgs e)
        {
            GL.BindVertexArray(_vertexArrayObject);
            _solidColorShader.Use();
            _solidColorShader.SetVector3("aInputColor", _color);
            _solidColorShader.SetMatrix4("model", _model);
            _solidColorShader.SetMatrix4("view", _window.Camera.GetViewMatrix());
            _solidColorShader.SetMatrix4("projection", _window.Camera.GetProjectionMatrix());
     
            GL.DrawElements(PrimitiveType.Triangles, _indices.Length, DrawElementsType.UnsignedInt, 0);
        }

        protected override void PrivateUpdate(FrameEventArgs e)
        {

            var transform = Matrix4.Identity;
            _time += e.Time;

            Yaw += (float)((MathHelper.Sin(_time) * 180f) * e.Time);
            if (Yaw > 360)
                Yaw = 0;

            Pitch = (float)MathHelper.Sin(_time) * 45f;

            Position += Front * 1.5f * (float)e.Time;

            transform = transform * Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(Pitch));
            transform = transform * Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(-Yaw) - MathHelper.PiOver2);
            transform = transform * Matrix4.CreateTranslation(Position);
            
            _model = transform;
        }
    }
}
