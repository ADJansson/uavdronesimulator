﻿using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using System;

namespace UAVSimulator_OpenTK
{
    public class Camera : WorldObject
    {
        private float _fov = MathHelper.PiOver2;

        public Camera(Window window, Vector3 position, float aspectRatio) : base (window)
        {
            Position = position;
            AspectRatio = aspectRatio;

            Yaw = -90;
        }

        public float AspectRatio { private get; set; }

        public float Fov
        {
            get => MathHelper.RadiansToDegrees(_fov);
            set
            {
                var angle = MathHelper.Clamp(value, 1f, 90f);
                _fov = MathHelper.DegreesToRadians(angle);
            }
        }

        public Matrix4 GetViewMatrix()
        {
            return Matrix4.LookAt(Position, Position + _front, _up);
        }

        public Matrix4 GetProjectionMatrix()
        {
            return Matrix4.CreatePerspectiveFieldOfView(_fov, AspectRatio, 0.01f, 100f);
        }

        protected override void PrivateLoad()
        {
        }

        protected override void PrivateRender(FrameEventArgs e)
        {          
        }

        protected override void PrivateUpdate(FrameEventArgs e)
        {          
        }
    }
}
